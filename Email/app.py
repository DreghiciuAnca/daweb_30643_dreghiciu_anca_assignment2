from flask import Flask
from flask import request
import smtplib, ssl
from flask_cors import CORS, cross_origin
from flask_mail import Mail, Message
import os
import jsonify

app = Flask(__name__)

app.config['CORS_HEADERS'] = 'application/json'
app.config['MAIL_SERVER'] = 'smtp.gmail.com'
app.config['MAIL_PORT'] = 465
app.config['MAIL_USERNAME'] = '' #gmail account
app.config['MAIL_PASSWORD'] = '' #password for gmail
app.config['MAIL_USE_TLS'] = False
app.config['MAIL_USE_SSL'] = True
mail = Mail(app)
CORS(app)


@app.route('/api', methods=['POST'])
@cross_origin()
def api():
    # form = ContactForm()
        print(request.method)
        if request.method == 'POST':
            content = request.json
            print(content)
            name = str(content['name'])
            message = str(content['message'])
            print(name)

        msg = Message(subject=name, sender='', recipients=[''], body=message) #sender =gmail acount recipients= another account
        mail.send(msg)
        return 'Message has been sent!'


if __name__ == '__main__':
    app.run()

